"use strict"

//1-
//функцію можна створити двома методами "function declaration" й "function expression". виклик функції відбувається дуе легко, пишеться name функції й після того круглі дужки.
//2-
// оператор return  повертає значення з функції. по факту він пишеться останній та на ньому функція завершується
//3-
//параметри у функції це фактично змінні які легко прописуються у круглих дужках.
//аргументи це значення функції яке ми в неї передали, прописується у тілі функції
//4-
//через функцію зворотнього виклику "call back"

//1-
const getResultOfDivision = function (one, two){
    return one / two
}
const resultOFDivision = getResultOfDivision(10,2);
console.log(resultOFDivision);

// 2-
const getUserNumber = function (string){
    let userNumber;
        do {
            userNumber = prompt(`add ${string} number`);
    } while (isNaN(userNumber));
        return +userNumber;
}

let userNumberOne = getUserNumber('first');
let userNumberTwo = getUserNumber('second');

let userOperation;

do {
    userOperation = prompt("add mathematical operation")

    if(userOperation !== "+" && userOperation !== "/" && userOperation !== "-" && userOperation !== "*"){
        alert("this is not mathematical operation! try again")
    }
} while (userOperation !== "+" && userOperation !== "/" && userOperation !== "-" && userOperation !== "*");

const calculator = function (){
    let result;
    if(userOperation === "+"){
        result = userNumberOne + userNumberTwo
    } else if (userOperation === "-") {
        result = userNumberOne - userNumberTwo
    } else if (userOperation === "*") {
        result = userNumberOne * userNumberTwo
    } else {
        result = userNumberOne / userNumberTwo
    }
    return result
}

console.log(calculator())

//3-
let userFactorial = getUserNumber('for factorial');
const factorial = (n = userFactorial) => {
    return (n !== 1) ? n * factorial(n - 1) : 1;
}
console.log(factorial());


